#! /usr/bin/env python

import json
import logging
import os
import time
from datetime import datetime, timedelta
import threading

import paho.mqtt.client as mqtt

from .config import MQTT_HOST, MQTT_PORT, MQTT_USERNAME, MQTT_PASSWORD, BASE_SETPOINT, SCHEDULES_PATH


logger = logging.getLogger(__name__)

def quit():
    wait = 10
    logger.info('Preparing to quit...')
    time.sleep(wait)
    logger.info('Quitting')
    os._exit(0)


def on_connect(client, userdata, flags, rc):
    logger.info('Connected with result code {}'.format(rc))

    run_schedules(client)
    logger.info('Schedules completed')

    quitter = threading.Thread(target=quit, args=())
    quitter.start()


def write_setpoint(client, unit, offset):
    ts = int(time.time() * 1000)
    value = float(offset) + BASE_SETPOINT
    payload = json.dumps({'timestamp': ts, 'value': value})
    client.publish('elnknx/{}/write'.format(unit), payload, 1)
    print('Unit: {}'.format(unit))
    print('Setpoint: {}'.format(value))

def run_schedules(client):
    for folder in os.listdir(SCHEDULES_PATH):
        if os.path.isfile(folder):
            continue

        path = os.path.join('schedules', folder)
        for file in os.listdir(path):
            if os.path.isdir(file):
                continue

            schedule = load_schedule(os.path.join(path, file))
            offset = get_closest_temperature(schedule)
            unit = folder + '/' + file.replace('.csv', '').replace('_', '/')

            if offset is not None:
                write_setpoint(client, unit, offset)


def load_schedule(filename):
    with open(filename, 'r') as f:
        schedule = []
        for line in f.readlines():
            setting = line.strip().split(',')
            schedule.append((setting[0], setting[1]))

        return schedule


def get_closest_temperature(schedule):
    # Finds the timestamp which is older and closest to the current time and returns its temperature
    now = datetime.now()
    closest = {"temperature": None, "diff": float('-inf')}
    for ts, temperature in schedule:
        hour, minute = ts.split(':')
        dt = now.replace(hour=int(hour), minute=int(minute))
        diff = (dt - now).total_seconds()

        # If time is greater than now we subtract a day
        if diff > 0:
            diff = (dt - timedelta(days=1) - now).total_seconds()

        if diff > closest["diff"]:
            closest = {"temperature": temperature, "diff": diff}

    return closest["temperature"]


def main():
    client = mqtt.Client()
    client.on_connect = on_connect

    client.username_pw_set(MQTT_USERNAME, MQTT_PASSWORD)
    client.connect(MQTT_HOST, MQTT_PORT, 60)
    client.loop_forever()
