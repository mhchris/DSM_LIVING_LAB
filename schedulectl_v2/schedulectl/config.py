# Hostname or IP address of MQTT broker to connect to
MQTT_HOST = 'mqtt.energydata.dk'

# Port to use when connecting to MQTT broker
MQTT_PORT = 1883

# Username to use when connecting to MQTT broker.
# Must be the Energydata.dk username associated with the token below.
MQTT_USERNAME = 's182360'

# Energydata.dk access token (Can be generated from 'My profile' -> 'Access tokens').
MQTT_PASSWORD = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQwOWY2N2ZkNmY1MDkyN2ZjMzcwMTEzYzJkYmQ0ZTNiZWE4ZDkxYjI5ZjczNjcxMDU4NDFmYWJhNzg4YzQwODZkY2RkOWU3M2FjZDg3ODg5In0.eyJhdWQiOiIxIiwianRpIjoiNDA5ZjY3ZmQ2ZjUwOTI3ZmMzNzAxMTNjMmRiZDRlM2JlYThkOTFiMjlmNzM2NzEwNTg0MWZhYmE3ODhjNDA4NmRjZGQ5ZTczYWNkODc4ODkiLCJpYXQiOjE1NDM0MTk1MjQsIm5iZiI6MTU0MzQxOTUyNCwiZXhwIjoxNTc0OTU1NTI0LCJzdWIiOiI1MSIsInNjb3BlcyI6W119.Rq6fDDFbmT7a0sMDEyDLZcx8GpbPw6X8Cmf3XQCDUS61UEIHGmUkKqBNAOxsihwheiw7OysjnohOdZGTYgcdimCH2YBxz3H0RU63uTwmQCbfbxyOWLtUCVp8f8JYXQw0zDXNmI3_dOFKhugabe9Qb81rlCFEoMvFiYaphNh52W17lB21dOefLwEBX0bkWhWFZX3V2dQPJvVEuOLVhUTFDgF8UJJnCnnbq08rNyRXCRyqzb_nKdXUlmoqJE1qLHcsHljog8O8fJbUd1j_TQrlJZSqYBZZu0Ie9A2-ZI9a4MJJ0tj0RBFv8dnA5fI-3RXbW6JB3ZRoqq4qsHTi0F_KBntyLsichfkqWmgJY9wk52jsEky-LZH2LnO-ifTZG3FC9adn_pu1bRUJTcWq8-vhMPyvlzkj6LWhzqajoeIT8ArZRuuaUUd1yYRsjFc71-ttECgi6XLTkX6R-Ff_3OJpQ-MC5lK9JrH77IoJDluZTNsCNT8loJrjowKcsrk9gsiSMr0i8-vjzvWQsQ_NxS0LkNQyQn07LkRIFHkgh0p3_TLlfo4zv1avbJ17c6cTjUZmU6jsgXLSFPziIjLD-FvmpwEIlBP_dgZZvo0xyqtackZQygSgB4R4LpzSwtNHe5x8bm7MKtkpyjbBGaDLfoFpWXEscOkWxYAyKqr4XHkHeF0'

# Base setpoint that offsets are applied to in order to find new setpoint
BASE_SETPOINT = 21

# Path to directory containing schedules. Can be absolute or relative.
SCHEDULES_PATH = 'schedules'

# Log level. Can be 'DEBUG', 'INFO', 'WARN', 'ERROR' or 'CRITICAL'
LOG_LEVEL = 'DEBUG'
