#! /usr/bin/env python

import logging
import sys

from schedulectl.main import main
from schedulectl.config import LOG_LEVEL


def configure_logging():
    log_level = logging.getLevelName(LOG_LEVEL)
    logging.basicConfig(
        stream=sys.stdout,
        level=log_level,
        format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )


if __name__ == '__main__':
    configure_logging()
    main()
