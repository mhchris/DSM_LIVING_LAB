# Schedulectl

This program controls floor heating setpoints according to schedules described in CSV files.
It uses the Energydata.dk MQTT broker to send messages to the ELN KNX interface,
which transform the message into a KNX telegram and forwards it to the KNX bus of the specified apartment.

## Installation

Schedulectl requires Python 3 and paho-mqtt. To set it up:

```bash
virtualenv -p python3 venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
```

## Configuration

The file `schedulectl/config.example.py` contains a skeleton config file. Copy that file to `schedulectl/config.py` and update the settings. The purpose of each setting is documented in the file.

## Schedules

Schedules are defined with a set of directories and files, e.g.:

```
schedules/
├── A33
│   ├── 2_6_1.csv
│   ├── 2_6_2.csv
└── A34
    ├── 2_6_1.csv
    └── 2_6_2.csv
```

A room is identified by its BasicSetPoint group address, e.g. 2/6/1.
The sample directory layout defines a schedule for the rooms 2/6/1 and 2/6/2 in the apartments A33 and A34.

The content of each CSV file must be a list of timestamps and temperature offsets, e.g.:

```
08:00,2
10:00,0
17:00,-2
```

This will apply an offset of 2 degrees from 08:00 to 10:00 and 0 degrees from 10:00 to 17:00. From 17:00 to 08:00 the offset will be -2 degrees, as the last offset is valid until the first offset.

## Running it

When Schedulectl is configured and you have defined a schedule, you can run the program. You will need to source the virtualenv, then run `schedulectl.py` with Python:

```bash
$ source venv/bin/activate
(venv) $ python schedulectl.py
```

When executed, Schedulectl reads all schedules, and for each schedule it sends a message to set the setpoint according to the offset. When it has sent a message for each schedule, it exits. This means it must be executed periodically (e.g. every 5 minutes) so it keeps updating the setpoint according to the schedule. This can be achieved with cron.
