# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 13:32:22 2019

@author: Lucas, Morten
"""
import os
import json
import time
import pandas as pd
import datetime
import paho.mqtt.client as mqtt
import os
import sys
import shutil

MQTT_HOST = 'mqtt.energydata.dk'
MQTT_PORT = 1883
MQTT_USERNAME = 'trobro'
MQTT_PASSWORD = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFlN2FkZGU4ZTc5MDEyZmQ4MDcyOWRkZDc2MWJhOGY3OWMwYjEyYTVjY2ZjZjIxYzU5NTI3YTQzYzNmYzA0NDA4MGEyN2NmN2RkNzk1ODllIn0.eyJhdWQiOiIxIiwianRpIjoiYWU3YWRkZThlNzkwMTJmZDgwNzI5ZGRkNzYxYmE4Zjc5YzBiMTJhNWNjZmNmMjFjNTk1MjdhNDNjM2ZjMDQ0MDgwYTI3Y2Y3ZGQ3OTU4OWUiLCJpYXQiOjE1Mzg2NTU5NjksIm5iZiI6MTUzODY1NTk2OSwiZXhwIjoxNTcwMTkxOTY5LCJzdWIiOiIyMyIsInNjb3BlcyI6W119.RnN9YY0k8R5miPENWlMfodS-znEBbGkVYuV-FtT4QK7omLwpMowAnuR_jVPGbi72vhEK9WP0yoXUJAuxF0FJOSMmrFg4TLaDdq7laavlNbcqhqPv7-vMVRh45CKZAOuJuQhYfhaDzORE0x4TicL-O1C_4ccTeqvJm_3df9s7OBhMK5kSiWyWf9wf5_tuH9KNh254PKdookq4H0oe08LtKKuuDV7mSfCtLZPY0npLrEQqqnHqS1VTY3QSbAjpVbwtkLUfKD3PZvscTPpAB_3jmjGdLrtOmtJdU70VamVK811yExWk7QCYjJTEjhDyPTERnvzbZAvAk8ssFaZA-M-sRvqsRtoaYBV4xSpV6L0jF8XLYUBbbvnPLTP52Xw8bcS3SNT0fBGwb_pkQTJu4mWYiYQNxaQY53ZkI7llPL-mBwyvI-W8kr6Zej_EdMmXTWM4Tw_Xik3CvnmaOSNh6RwzUqRKR6VHb8SLrcCHnQ_UBDwSXi--qupAX-m0YqGNwFTJCK9FC6lyxGaBNn3jHJD1gMoHvXoizC9dqyladwe2ZzJYWgRiHvq3ttLhG10txophX0Y8oRVhAVZ9w9zIimqXr_zRPFUR5mCv5ZG6oGfgvKpsn2qx1q2lGzfZvcUB8_tMHqVUi8Z-1Qs7CSV73fgox2oMm6Wdd9UDAtlt5bmBnQA'
DISPLAY_PATH = 'metadata/Schedules'
OUTPUT_PATH = 'schedules'
current_setpoints = {}

def on_connect(client, userdata, flags, rc):
    print('Connected with result code {}'.format(rc))
    subscribe_to_topics(client)
    print('Subscribed to topics')

def on_message(client, userdata, msg):
    topic = msg.topic
    payload = json.loads(msg.payload)
    if payload['value'] != 0:
        print('Topic: {}'.format(topic))
        print('Display: {}'.format(payload['value']))
        data = {topic : payload['value']}
        current_setpoints.update(data)

def subscribe_to_topics(client):
    for folder in os.listdir(DISPLAY_PATH):
        for file in os.listdir(DISPLAY_PATH + '/' + folder):
            topic = folder + '_' + file
            topic = topic.replace('.csv', '').replace('_', '/')
            client.subscribe('elnknx/{}'.format(topic), 1)

def request_setpoints(client):
    for folder in os.listdir(DISPLAY_PATH):
        for file in os.listdir(DISPLAY_PATH + '/' + folder):
            topic = folder + '_' + file
            topic = topic.replace('.csv', '').replace('_', '/')
            ts = int(time.time() * 1000)
            payload = json.dumps({'timestamp': ts, 'value': 1})
            address = 'elnknx/{}'.format(topic) + '/read'
            client.publish(address, payload, 1)
            print('Message published to ' + address)
            time.sleep(1)

def data_to_json(data):
    with open('data.json', 'w') as fp:
        json.dump(data, fp, sort_keys=True, indent=4)

def json_to_csv():
    preheating_start = '3:45'
    preheating_finish = '6:00'
    experiment_finish = '12:00'

    setpoint_reference = 21
    preheating_offset = 0
    peak_offset = 1.5
    peak_setpoint = 18

    with open('data.json') as f:
        data = json.load(f)
        for key, value in data.items():
            #check if value is less then peak value
            folder_name = key[7:10]
            # Create output directory if don't exist:
            if not os.path.exists(OUTPUT_PATH):
                os.mkdir(OUTPUT_PATH)
            if not os.path.exists(OUTPUT_PATH + '/' + folder_name):
                os.mkdir(OUTPUT_PATH + '/' + folder_name)
            # Create csv files:
            file_name = key[11:17] + '.csv'
            file_name = file_name.replace('/', '_')
            if '2_2_1' in file_name:
                row1 = [preheating_start, (value + preheating_offset - setpoint_reference)]
            else:
                row1 = [preheating_start, (value - setpoint_reference)]
            #check if value is less then peak value, then keep original
            if value < peak_setpoint:
                row2 = [preheating_finish, value - setpoint_reference]
            else:
            #    row2 = [preheating_finish, peak_setpoint - setpoint_reference]
                row2 = [preheating_finish, value - setpoint_reference - peak_offset]
            #sequence ending times to limit peak effects
            s = datetime.datetime.strptime(experiment_finish, "%H:%M")
            s = s + datetime.timedelta(minutes=1)
            experiment_finish = str(s)[11:-3] 
            row3 = [experiment_finish, value - setpoint_reference]
            #replace 2 with 6 in adress to set to basic setpoint instead of display setpoint
            file_name = file_name[:2] + '6' + file_name[3:]
            d = [row1, row2, row3]
            df = pd.DataFrame(data=d).set_index(0)
            df.to_csv(OUTPUT_PATH + '/' + folder_name + '/' + file_name, header=False)

def main():
    try:
        shutil.rmtree(OUTPUT_PATH)
    except OSError as e:
        print ("Error: %s - %s." % (e.filename, e.strerror))


    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.username_pw_set(MQTT_USERNAME, MQTT_PASSWORD)
    client.connect(MQTT_HOST, MQTT_PORT, 60)
    client.loop_start()
    time.sleep(10)
    #input('>>> Press enter when ready...' + os.linesep)
    request_setpoints(client)
    data_to_json(current_setpoints)
    json_to_csv()
    #input('>>> Task completed, press enter to quit...' + os.linesep)

main()
